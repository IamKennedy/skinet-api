﻿using API.Errors;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("errors/[controller]")]
    public class ErrorController : BaseApiController
    {
        [Route("errors/{code}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ActionResult Error(int code) 
        {
            return new ObjectResult(new ApiResponse(code));
        }
    }
}
