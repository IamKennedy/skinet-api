﻿using AutoMapper;
using Core.DTOS;
using Core.Entities;
using Microsoft.Extensions.Configuration;
using System;

namespace API.Helpers
{
    public class ProductUrlResolver : IValueResolver<Product, ProductDto, string>
    {
        private readonly IConfiguration config;

        public ProductUrlResolver(IConfiguration config)
        {
            this.config = config;
        }

        public string Resolve(Product source, ProductDto destination, string destMember, ResolutionContext context)
        {
            if (!string.IsNullOrEmpty(source.PictureUrl)) 
            {
                return config["ApiUrl"] + source.PictureUrl;
            }

            return null;
        }
    }
}
