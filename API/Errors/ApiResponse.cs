﻿using System;

namespace API.Errors
{
    public class ApiResponse
    {
        public ApiResponse(int statusCode, string message = null)
        {
            this.StatusCode = statusCode;

            this.Message = message ?? GetDefaultMessageForStatusCode(statusCode);     
        }

        public int StatusCode { get; set; }
        public string Message { get; set; }
       

        private string GetDefaultMessageForStatusCode(int statusCode)
        {
            return statusCode switch
            {
                400 => "You have made a bad request",
                401 => "You are not authorized to access this resource",
                404 => "Resource Not Found",
                500 => "Something went terribly wrong with the server",
                _ => null
            };
        }
    }
}
