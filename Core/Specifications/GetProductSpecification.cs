﻿using Core.Entities;

namespace Core.Specifications
{
    public class GetProductSpecification : BaseSpecification<Product>
    {
        public GetProductSpecification(int id)
        {
            Criteria = p => p.Id == id;

            AddInclude(p => p.ProductBrand);

            AddInclude(p => p.ProductType);
        }
    }
}
